//
//  WKWebViewExtension.swift
//  SlothAdsDemo
//
//  Created by Peerasak Unsakon on 27/8/2564 BE.
//

import WebKit

extension WKWebView {
    
    func stringByEvaluatingJavaScript(from script: String) -> Any?{
        var resultString : Any? = nil
        var finished = false
        self.evaluateJavaScript(script) { (result, error) in
            if error == nil{
                if result != nil{
                    resultString = result
                }
            }else{
                
            }
            
            finished = true
        }
        
        while !finished {
            RunLoop.current.run(mode: .default, before: Date.distantFuture)
        }
        
        return resultString
    }
    
    func contentSize(completion : @escaping (_ size: CGSize?, _ error: Error?) -> ()){
        self.evaluateJavaScript("document.body.scrollHeight;") { (scrollHeight, error) in
            if let height = scrollHeight as? CGFloat {
                self.evaluateJavaScript("document.body.scrollWidth;") { (scrollWidth, error) in
                    if let width = scrollWidth as? CGFloat {
                        let size = CGSize.init(width: width, height: height)
                        completion(size, nil)
                    } else {
                        completion(nil, error)
                    }
                }
            } else {
                completion(nil, error)
            }
        }
    }
}
