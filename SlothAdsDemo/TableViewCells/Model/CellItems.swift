//
//  Article.swift
//  SlothAdsDemo
//
//  Created by Peerasak Unsakon on 27/8/2564 BE.
//

import WebKit
import Foundation
import Ookbee_Mobile_Ads_SDK

protocol WebArticleItemDelegate: NSObjectProtocol {
    func didUpdateContentHeight(height: CGFloat, indexPath: IndexPath)
}

struct HeaderArticleItem {
    var title: String
    var author: String
    var category: String
    var createdAt: Date
    var authorImg: UIImage = UIImage(named: "logo_korkow")!
}

class WebArticleItem: NSObject, WKNavigationDelegate {
    
    var html: String!
    var webview: WKWebView?
    var indexPath: IndexPath?
    var contentHeight: CGFloat = 200
    var readerViewController: ArticleReaderViewControllerViewController!
    
    weak var delegate: WebArticleItemDelegate?
    
    init(html: String, on viewcontroller: ArticleReaderViewControllerViewController) {
        self.html = html
        self.readerViewController = viewcontroller
    }
    
    func setItemIndex(indexPath: IndexPath) {
        self.indexPath = indexPath
    }
    
    func createWebView() {
        let jscript = """
            var meta = document.createElement('meta');
            meta.setAttribute('name', 'viewport');
            meta.setAttribute('content', 'width=device-width');
            document.getElementsByTagName('head')[0].appendChild(meta);
        """
        
        let userScript = WKUserScript(source: jscript, injectionTime: .atDocumentEnd, forMainFrameOnly: true)
        let wkUController = WKUserContentController()
        wkUController.addUserScript(userScript)
        let wkWebConfig = WKWebViewConfiguration()
        wkWebConfig.userContentController = wkUController
        
        let newWebView = WKWebView(frame: CGRect.zero, configuration: wkWebConfig)
        newWebView.navigationDelegate = self
        newWebView.frame = CGRect(x: 0, y: 0, width: self.readerViewController.view.frame.width, height: self.contentHeight)
        newWebView.scrollView.isScrollEnabled = false
        
        self.webview = newWebView
        self.webview?.navigationDelegate = self
        self.renderHtml(html: self.html)
    
    }
    
    private func renderHtml(html: String) {
        let template = "Style2"
        
        let bundle = Bundle.main
        let path: String? = bundle.path(forResource: template, ofType: "css")
        let url: URL? = path != nil ? URL(fileURLWithPath: path!) : nil

        let head = """
        <head>
            <link rel="stylesheet" type="text/css" href="\(template).css">
        </head>
        """
        
        self.webview?.loadHTMLString("\(head)\(html)", baseURL: url)
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        webView.contentSize { [weak self] size, error in
            if let size = size {
                self?.contentHeight = size.height
                if let indexPath = self?.indexPath {
                    print("APP: update naa - \(size.height), at index: \(indexPath)")
                    self?.delegate?.didUpdateContentHeight(height: size.height, indexPath: indexPath)
                }
            }
        }
    }
    
}

struct ArticleAdsItem {
    var bannerAd: BannerAd
}

