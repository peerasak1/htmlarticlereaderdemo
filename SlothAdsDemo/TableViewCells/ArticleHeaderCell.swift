//
//  ArticleHeaderCell.swift
//  SlothAdsDemo
//
//  Created by Peerasak Unsakon on 30/08/2021.
//

import UIKit

class ArticleHeaderCell: UITableViewCell, ConfigurableCell {
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 3
        label.font = UIFont(name: "Mitr-Medium", size: 18)
        return label
    }()
    
    lazy var catView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 10
        view.layer.borderWidth = 1
        view.layer.borderColor = UIColor.black.cgColor
        view.backgroundColor = .clear
       return view
    }()
    
    lazy var catLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 1
        label.font = UIFont(name: "Mitr-Regular", size: 16)
        return label
    }()
    
    lazy var logoImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.layer.cornerRadius = 15
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    lazy var authorLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 1
        label.font = UIFont(name: "Mitr-Regular", size: 16)
        return label
    }()
    
    lazy var datetimeLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 1
        label.font = UIFont(name: "Mitr-Regular", size: 16)
        return label
    }()
    
    lazy var line: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.gray
        return view
    }()
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.addSubview(self.titleLabel)
        self.addSubview(self.catView)
        self.addSubview(self.logoImageView)
        self.addSubview(self.authorLabel)
        self.addSubview(self.datetimeLabel)
        self.addSubview(self.line)
        
        self.catView.addSubview(self.catLabel)
        
        self.titleLabel.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
            make.top.equalToSuperview().offset(20)
            make.bottom.equalTo(self.catView.snp.top).offset(-8)
        }
        
        self.catView.snp.makeConstraints { make in
            make.leading.equalTo(self.titleLabel)
            make.bottom.equalTo(self.line.snp.top).offset(-10)
        }
        
        self.line.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
            make.bottom.equalToSuperview().inset(20)
            make.height.equalTo(0.5)
        }
        
        self.catLabel.snp.makeConstraints { make in
            make.leading.equalToSuperview().inset(6)
            make.trailing.equalToSuperview().inset(6)
            make.top.equalToSuperview().inset(6)
            make.bottom.equalToSuperview().inset(6)
        }
        
        self.logoImageView.snp.makeConstraints { make in
            make.left.equalTo(self.catView.snp.right).offset(10)
            make.centerY.equalTo(self.catView.snp.centerY)
            make.width.equalTo(30)
            make.height.equalTo(30)
        }
        
        self.authorLabel.snp.makeConstraints { make in
            make.left.equalTo(self.logoImageView.snp.right).offset(10)
            make.centerY.equalTo(self.catView.snp.centerY)
        }
        
        self.datetimeLabel.snp.makeConstraints { make in
            make.left.equalTo(self.authorLabel.snp.right).offset(10)
            make.centerY.equalTo(self.catView.snp.centerY)
        }
        
    }

    public func willDisplay() {
        
    }
    
    public func configure(data: HeaderArticleItem) {
        self.titleLabel.text = data.title
        self.catLabel.text = data.category
        self.authorLabel.text = data.author
        self.datetimeLabel.text = data.createdAt.toString()
        self.logoImageView.image = data.authorImg
    }
    
    public func didEndDisplaying() {
        self.titleLabel.text = nil
        self.catLabel.text = nil
        self.authorLabel.text = nil
        self.datetimeLabel.text = nil
        self.logoImageView.image = nil
    }
    
}

extension Date {
    
    func toString() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/YYYY"
        return formatter.string(from: self)
    }
    
}
