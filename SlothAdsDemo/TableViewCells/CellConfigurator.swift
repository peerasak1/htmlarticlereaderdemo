//
//  CellConfigurator.swift
//  SlothAdsDemo
//
//  Created by Peerasak Unsakon on 30/08/2021.
//

import UIKit

protocol ConfigurableCell {
    associatedtype DataType
    static var reuseIdentifier: String { get }
    func configure(data: DataType)
    func willDisplay()
    func didEndDisplaying()
}

extension ConfigurableCell {
    static var reuseIdentifier: String { return String(describing: Self.self) }
}

protocol CellConfigurator {
    static var reuseId: String { get }
    func configure(cell: UIView)
}

class TableCellConfigurator<CellType: ConfigurableCell, DataType>: CellConfigurator where CellType.DataType == DataType, CellType: UITableViewCell {
    
    static var reuseId: String { return CellType.reuseIdentifier }
    
    let item: DataType
    
    init(item: DataType) {
        self.item = item
    }
    
    func configure(cell: UIView) {
        guard let cellView = cell as? CellType else { return }
        cellView.configure(data: self.item)
    }
    
}
