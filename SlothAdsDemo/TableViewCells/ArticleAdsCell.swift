//
//  ArticleAdsCell.swift
//  SlothAdsDemo
//
//  Created by Peerasak Unsakon on 30/8/2564 BE.
//

import UIKit
import SnapKit
import Ookbee_Mobile_Ads_SDK

class ArticleAdsCell: UITableViewCell, ConfigurableCell {

    var adView: UIView?
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
    }

    public func willDisplay() {
        
    }
    
    public func configure(data: ArticleAdsItem) {
        
        if self.adView == nil  {
            self.adView = data.bannerAd.loadAdsView()
        }
        
        guard let adView = self.adView else { return }
        
        self.backgroundColor = .systemYellow
        self.contentView.addSubview(adView)
        
        adView.snp.makeConstraints { make in
            make.leading.equalToSuperview().inset(10)
            make.top.equalToSuperview().inset(34)
            make.trailing.equalToSuperview().inset(10)
            make.bottom.equalToSuperview().inset(34)
            make.height.equalTo(276)
        }
    }
    
    public func didEndDisplaying() {
        self.contentView.subviews.forEach { (view) in
            view.removeFromSuperview()
        }
    }
    
}

