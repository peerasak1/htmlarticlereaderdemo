//
//  ArticleContentCell.swift
//  SlothAdsDemo
//
//  Created by Peerasak Unsakon on 30/08/2021.
//

import UIKit
import WebKit
import SnapKit

class ArticleContentCell: UITableViewCell, ConfigurableCell {
  
    var webview: WKWebView?
    var contentHeight: CGFloat?
    var articleItem: WebArticleItem!
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }

    public func configure(data: WebArticleItem) {
        self.articleItem = data
        self.articleItem.delegate = self
        self.webview = data.webview
        guard
            let contentWebView = self.webview
        else { return }
        
        self.contentView.addSubview(contentWebView)
        contentWebView.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.leading.equalToSuperview()
            make.trailing.equalToSuperview()
            make.bottom.equalToSuperview()
        }
    }
    
    public func willDisplay() {

    }
    
    public func didEndDisplaying() {
        self.contentView.subviews.forEach { (view) in
            view.removeFromSuperview()
        }
    }
     
    private func updateWebView(height: CGFloat) {
        guard let _ = self.webview?.superview else { return }
        
        self.contentHeight = height
        
        
        guard let itemIndexPath = self.articleItem.indexPath else { return }
        self.articleItem.readerViewController.tableView.reloadRowWithoutAnimation(itemIndexPath)
    }
}

extension ArticleContentCell: WebArticleItemDelegate {
    
    func didUpdateContentHeight(height: CGFloat, indexPath: IndexPath) {
        if self.articleItem.indexPath == indexPath {
            print("APP: update webView at index: \(indexPath) with height: \(height)")
            self.updateWebView(height: height)
        }
    }
    
}
