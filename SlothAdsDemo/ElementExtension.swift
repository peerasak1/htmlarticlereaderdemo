//
//  ElementExtension.swift
//  SlothAdsDemo
//
//  Created by Peerasak Unsakon on 27/8/2564 BE.
//

import SwiftSoup

extension Element {
    
    func updateImageWidth() throws -> Element {
        if let imageTag: Element = try self.getElementsByTag("img").first() {
            try imageTag.attr("width", "100%")
        }
        return self
    }
    
}
