//
//  ArticleReaderViewControllerViewController.swift
//  SlothAdsDemo
//
//  Created by Peerasak Unsakon on 30/08/2021.
//

import UIKit

class ArticleReaderViewControllerViewController: UIViewController {
    
    lazy var tableView: UITableView = {
        var tableView = UITableView()
        tableView.separatorStyle = .none
        tableView.backgroundColor = .white
        tableView.allowsSelection = false
        tableView.estimatedRowHeight = 100
        
        return tableView
    }()
    var presenter: ArticleReaderViewControllerPresenter!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.presenter.delegate = self
        self.presenter.fetchData()

        self.tableView.register(ArticleHeaderCell.self, forCellReuseIdentifier: "ArticleHeaderCell")
        self.tableView.register(ArticleContentCell.self, forCellReuseIdentifier: "ArticleContentCell")
        self.tableView.register(ArticleAdsCell.self, forCellReuseIdentifier: "ArticleAdsCell")
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.estimatedRowHeight = 100
        self.tableView.tableFooterView = UIView()
        
        self.view.addSubview(self.tableView)
        
        self.tableView.snp.makeConstraints { make in
            make.leading.equalToSuperview()
            make.top.equalToSuperview()
            make.trailing.equalToSuperview()
            make.bottom.equalToSuperview()
        }
    }
}

extension ArticleReaderViewControllerViewController: ArticleReaderViewControllerDelegate {
    
    func reloadTableView() {
        print("APP: reloadView La")
        self.tableView.reloadData()
    }
    
}

extension ArticleReaderViewControllerViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.numberOfItems
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = presenter.item(at: indexPath)
        let cell = tableView.dequeueReusableCell(withIdentifier: type(of: item).reuseId)!
        item.configure(cell: cell)

        return cell
    }
}

extension ArticleReaderViewControllerViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let contentCell = cell as? ArticleContentCell else { return }
        contentCell.didEndDisplaying()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let item = presenter.item(at: indexPath)
        if let cellConfig = item as? ArticleContentCellConfig {
            return cellConfig.item.contentHeight
        }
        
        return UITableView.automaticDimension
    }
}
