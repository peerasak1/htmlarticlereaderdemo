//
//  ModeSelectorViewController.swift
//  SlothAdsDemo
//
//  Created by Peerasak Unsakon on 30/8/2564 BE.
//

import UIKit

class ModeCell: UITableViewCell {
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        return label
    }()
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.addSubview(self.titleLabel)
        
        self.titleLabel.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
            make.top.equalToSuperview().offset(20)
            make.bottom.equalToSuperview().inset(20)
        }
   
    }
    
    func configure(title: String) {
        self.titleLabel.text = title
    }
    
}


class ModeSelectorViewController: UITableViewController {

    enum Mode: Int {
        case top = 0
        case bottom = 1
        case both = 2
        case paragraph = 3
        
        var title: String {
            switch self {
            case .top:
                return "Show banner on Top of article"
            case .bottom:
                return "Show banner on Bottom of article"
            case .both:
                return "Show banner on Top and Bottom of article"
            case .paragraph:
                return "Show banner in The middle of article"
            }
        }
    
    }
    
    var numberOfRows: Int = 4
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.allowsSelection = true
        tableView.estimatedRowHeight = 80
        tableView.separatorStyle = .singleLine
        tableView.register(ModeCell.self, forCellReuseIdentifier: "ModeCell")
        title = "Demo WebView with Ads"
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.numberOfRows
    }


    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ModeCell", for: indexPath) as? ModeCell else {
            fatalError("No Cell")
        }
        
        let mode = Mode(rawValue: indexPath.row)!
        cell.configure(title: mode.title)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let mode = Mode(rawValue: indexPath.row)!
        self.performSegue(withIdentifier: "ShowArticle", sender: mode)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard
            let mode = sender as? Mode,
            let readerViewController = segue.destination as? ArticleReaderViewControllerViewController
        else {
         return
        }
        
        readerViewController.presenter = ArticleReaderViewControllerPresenter(for: readerViewController, mode: mode)
    }
}
