//
//  ArticleReaderViewControllerPresenter.swift
//  SlothAdsDemo
//
//  Created by Peerasak Unsakon on 30/08/2021.
//

import Foundation
import SwiftSoup
import WebKit
import PromiseKit
import Ookbee_Mobile_Ads_SDK

typealias HeaderArticleCellConfig = TableCellConfigurator<ArticleHeaderCell, HeaderArticleItem>
typealias ArticleContentCellConfig = TableCellConfigurator<ArticleContentCell, WebArticleItem>
typealias ArticleAdsCellConfig = TableCellConfigurator<ArticleAdsCell, ArticleAdsItem>

protocol ArticleReaderViewControllerDelegate: NSObjectProtocol {
    func reloadTableView()
}

public class ArticleReaderViewControllerPresenter {
    
    var cellConfigItems: [CellConfigurator] = []
    var numberOfItems: Int {
        return cellConfigItems.count
    }
    
    var viewcontroller: ArticleReaderViewControllerViewController!
    var mode: ModeSelectorViewController.Mode = .top
    
    weak var delegate: ArticleReaderViewControllerDelegate?
    
    /// MARK: - Presenter Private functions -
    
    private func parseHtml(whitelistTags: [String]) -> [String] {
        do {
            let doc: Document = try SwiftSoup.parse(htmlString)
            let elements: Elements = try doc.getAllElements()
            let filtered = elements.filter({ whitelistTags.contains($0.tagName()) })
            
            let htmls = filtered.map { (element: Element) -> String in
                do {
                    return try element.outerHtml()
                } catch {
                    return ""
                }
            }
            
            return htmls.filter({ $0 != "" })
        } catch {
            return []
        }
    }
    
    private func isIn(modes: [ModeSelectorViewController.Mode]) -> Bool {
        return modes.contains(self.mode)
    }
    
    private func setupContentWithAd(htmls: [String], bannerAd: BannerAd? = nil) {
        
        self.cellConfigItems.append(HeaderArticleCellConfig(item: HeaderArticleItem(title: "DOD เดินเกมรุกครึ่งปีหลัง ลุยกัญชงเต็มสูบ", author: "คอข่าว", category: "ธุรกิจ", createdAt: Date())))
        
        if self.isIn(modes: [.top,.both,.paragraph]), let bannerAd = bannerAd {
            self.cellConfigItems.append(bannerAd: bannerAd)
        }
        
        var articleCellConfigs = htmls.map { html -> CellConfigurator in
            return ArticleContentCellConfig(item: WebArticleItem(html: html, on: self.viewcontroller))
        }
        
        if self.isIn(modes: [.paragraph]), let bannerAd = bannerAd {
            articleCellConfigs.insertAdsBetweenContent(bannerAd: bannerAd)
        }
    
        self.cellConfigItems += articleCellConfigs
        
        if self.isIn(modes: [.bottom, .both, .paragraph]), let bannerAd = bannerAd {
            self.cellConfigItems.append(bannerAd: bannerAd)
        }
        
        self.cellConfigItems.enumerated().forEach { (index, cellConfig) in
            if let articleConfigCell = cellConfig as? ArticleContentCellConfig {
                articleConfigCell.item.setItemIndex(indexPath: IndexPath(row: index, section: 0))
                articleConfigCell.item.createWebView()
            }
        }
        
        self.delegate?.reloadTableView()
    }
    
    /// MARK: - Presenter Interfaces -
    
    init(for viewcontroller: ArticleReaderViewControllerViewController, mode: ModeSelectorViewController.Mode) {
        self.viewcontroller = viewcontroller
        self.mode = mode
    }
    
    func item(at indexPath: IndexPath) -> CellConfigurator {
        return cellConfigItems[indexPath.item]
    }
    
    func fetchData() {
        
        let whitelistTags: [String] = ["figure","p"]
        let htmls: [String] = self.parseHtml(whitelistTags: whitelistTags)
        
        let adLoader = BannerAdLoader(
            groupId: 2,
            ookbeeId: "",
            palceholder: nil,
            rootViewController: self.viewcontroller
        )
        
        adLoader.promiseRequest()
            .done { (bannerAd: BannerAd) in
                self.setupContentWithAd(htmls: htmls, bannerAd: bannerAd)
            }.catch { error in
                self.setupContentWithAd(htmls: htmls)
            }
        
    }
    
}

extension BannerAdLoader {
    
    func promiseRequest() -> Promise<BannerAd> {
        return Promise { seal in
            self.requestAds { (bannerAd, error) in
                if let err = error {
                    seal.reject(err)
                    return
                }
                if let bannerAd = bannerAd {
                    seal.fulfill(bannerAd)
                }else {
                    seal.reject(AdNetworkAdapterError.loadFail)
                }
            }
        }
    }
    
}

extension Array where Element == CellConfigurator {
    
    mutating func append(bannerAd: BannerAd) {
        self.append(ArticleAdsCellConfig(item: ArticleAdsItem(bannerAd: bannerAd)))
    }
    
    mutating func insertAdsBetweenContent(bannerAd: BannerAd) {
        let middleIndex = Int(self.count/2)
        self.insert(ArticleAdsCellConfig(item: ArticleAdsItem(bannerAd: bannerAd)), at: middleIndex)
    }
    
}
