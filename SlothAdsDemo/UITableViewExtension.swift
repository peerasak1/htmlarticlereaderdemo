//
//  UITableViewExtension.swift
//  SlothAdsDemo
//
//  Created by Peerasak Unsakon on 27/8/2564 BE.
//

import UIKit

extension UITableView {
    
    func reloadRowWithoutAnimation(_ indexPath: IndexPath) {
        let lastScrollOffset = contentOffset
        UIView.performWithoutAnimation {
            reloadRows(at: [indexPath], with: .none)
        }
        setContentOffset(lastScrollOffset, animated: false)
        print("APP: reload at index: \(indexPath)")
    }
    
}
